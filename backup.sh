#!/bin/bash

OIFS="$IFS"
IFS=$'\n'

# print help
if [ "$1" = '-h' ] || [ "$1" = '--help' ]
then
  echo "
  This script is meant to backup a directory.
  It does it by creating a version dir (YYYYMMDDHHMMSS) in the target dir and copies changed files with linking if file has not changed.
  The most recent version is also stored in 'last-version' dir in the backup dir for an easy access (it consists of links to the most recent version files).

  The source, target and remote confg variables can be defined in .config file, otherwise they should be provided as in the example below.
  The remote configuration is optional. If it's not provided, the backup will not be synchronised with remote.

  Usage:
  ./backup.sh [SOURCE_DIRECTORY] [TARGET_DIRECTORY] [?REMOTE_HOST] [?REMOTE_DIRECTORY]
  "
  exit 0
fi;

# get source, target and remote directories, and trim trailing slashes
source_dir=$(echo $1 | sed 's:/*$::')
target_dir=$(echo $2 | sed 's:/*$::')
remote_host=$(echo $3 | sed 's:/*$::')
remote_dir=$(echo $4 | sed 's:/*$::')

. ./.config

# verify both directories are provided
if [ -z "$source_dir" ] || [ -z "$target_dir" ]
then
  echo "Source and target directories are required"
  exit 1
fi;

[ ! -d "$source_dir" ] && echo "Source directory doesn't exist" && exit 1
[ ! -d "$target_dir" ] && echo "Target directory doesn't exist" && exit 1

# setup backup directory variables
backup_date=$(date +%Y%m%d%H%M%S)
current_version_dir="$target_dir/$backup_date"
last_version_dir="$target_dir/last-version"
tmp_last_version_dir="$target_dir/tmp-last-version"

# create last version dir if not exists
[ ! -d "$last_version_dir" ] && mkdir "$last_version_dir"

#  reate new version dir
[ -d "$current_version_dir" ] && echo "Version already exists" && exit 0

echo "Creating version '$backup_date'"

mkdir "$current_version_dir"
mkdir "$tmp_last_version_dir"

files_to_backup=$(find "$source_dir" -type f)

# print number of files being backed
no_of_files=$(find "$source_dir" -type f | wc -l)

if [ no_of_files = "1" ]
then
  echo "Backing up 1 file..."
else
  echo "Backing up $no_of_files files..."
fi;

progress=0

# for each file in source directory
for file in $files_to_backup
do
  last_version_file=${file/$source_dir/$last_version_dir}
  current_version_file=${file/$source_dir/$current_version_dir}
  tmp_last_version_file=${file/$source_dir/$tmp_last_version_dir}

  # copy the file if it's new or it's different from the last version
  current_version_file_location=$(dirname "$current_version_file")
  mkdir -p "$current_version_file_location"
  if [ ! -f "$last_version_file" ] || ! cmp -s "$last_version_file" "$file"
  then
    rsync -qztogpEADX "$file" "$current_version_file"
  else
    # link file to the previous version if it is the same
    ln -sr "$last_version_file" "$current_version_file"
  fi;

  # create a link to the file in tmp last version folder
  tmp_last_version_file_location=$(dirname "$tmp_last_version_file")
  mkdir -p "$tmp_last_version_file_location"
  ln -sr "$current_version_file" "$tmp_last_version_file"

  # log progress
  progress=$(($progress+1))
  echo -ne "\rBacked up $progress/$no_of_files files..."
done

IFS="$OIFS"

# move tmp last version to last version
rm -rf "$last_version_dir"
mv "$tmp_last_version_dir" "$last_version_dir"
rm -rf "$tmp_last_version_dir"

# exit if remote host not provided
[ -z "$remote_host" ] && printf "\n\nRemote host not provided\nLocal backup finished\n\n" && exit 0;

# use source directory name if remote directory name is not provided
if [ -z "$remote_dir" ]
then
  printf "\n\nRemote directory not provided, using source directory name\n"
  remote_dir=$(basename "$source_dir")
fi;

# create remote directory if not exists
printf "Checking remote directory...\n" \
  && ssh "$remote_host" test -d "$remote_dir" \
  || printf "Remote directory doesn't exist, creating now...\n" \
  && ssh "$remote_host" "mkdir -p '$remote_dir'"

# synch backup directory with remote
printf "Synchronising backup directory with remote directory...\n" \
  && rsync -aqtEADX "$target_dir/" "$remote_host":"$remote_dir/" \
  && printf "Synchronised\n"
